﻿using System.Text.Json.Serialization;

namespace PharmacyApp.Data.Dto.SuklApi.RequestQuery
{
    public class FieldFilter{}
    public class WhereFilter{}
    public class PageFilter<F, W>
       where F : FieldFilter 
       where W : WhereFilter 
    {
        [JsonPropertyName("limit")]
        public int Limit { get; set; } = 0;

        [JsonPropertyName("skip")]
        public int Skip => Page * Limit;
           
        [JsonIgnore]
        public int Page { get; set; } = 0;

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("fields")]
        public F? Fields { get; set; } = null;

        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        [JsonPropertyName("where")]
        public W? Where { get; set; } = null;

    }
}
