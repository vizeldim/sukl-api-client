﻿using PharmacyApp.Data.Dto.SuklApi;
using System.Text.Json.Serialization;

namespace PharmacyApp.Data.Dto
{
    public class SuklDrugDto : SuklBaseDto
    {
        [JsonPropertyName("NAZEV")]
        public string Name { get; set; } = default!;

        [JsonPropertyName("KOD_SUKL")]
        public string SuklCode { get; set; } = default!;

    }
}
