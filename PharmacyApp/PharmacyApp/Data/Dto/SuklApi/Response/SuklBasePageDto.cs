﻿namespace PharmacyApp.Data.Dto.SuklApi
{
    public class SuklBasePageDto<T> where T : SuklBaseDto
    {
        public List<T> Data { get; set; } = new();
    }
}
