﻿
using PharmacyApp.Data.Dto;
using PharmacyApp.Data.Dto.SuklApi;
using PharmacyApp.Data.Dto.SuklApi.RequestFilter;
using Refit;

// TODO: Proceed some editional info (like status code) instead of null on error
namespace PharmacyApp.Data.External.SuklApi
{
    [Headers("Accept: application/json")]
    public interface ISuklApiDescription
    {
        [Get("/dlp-lecivepripravky")]
        Task<ApiResponse<SuklDrugPageDto>> GetDrugPage([Query] string filter);
    }

    public class SuklClient : IDrugApiClient
    {
        private readonly ISuklApiDescription _suklApi;

        public SuklClient(ISuklApiDescription suklApi)
        {
            _suklApi = suklApi;
        }

      
        public async Task<DrugPageDto?> GetDrugPage(int page = 0, int size = 20)
        {
            DrugPageFilter filter = new()
            {
                Limit = size,
                Page = page,
                Fields = new(),
                Where = null
            };


            var response = await _suklApi.GetDrugPage(filter.ToQueryString());


            return response?.Content?.ToDto();
        }

        public async Task<DrugPageDto?> GetDrugPageByName(string name, int page = 0, int size = 10)
        {
            DrugPageFilter filter = new()
            {
                Limit = size,
                Page = page,
                Fields = new(),
                Where = new()
                {
                    Name = name
                }
            };

            var response = await _suklApi.GetDrugPage(filter.ToQueryString());

            return response?.Content?.ToDto();
        }
        public async Task<DrugDto?> GetFirstDrugByName(string name)
        {
            var found = await GetDrugPageByName(name);
            
            return found?.Data?.FirstOrDefault();
        }
        
 

        public async Task<DrugDto?> GetDrugBySuklCode(string suklCode)
        {
            DrugPageFilter filter = new()
            {
                Fields = new(),
                Where = new()
                {
                    SuklCode = suklCode
                }
            };

            var response = await _suklApi.GetDrugPage(filter.ToQueryString());
            SuklDrugDto? found = response?.Content?.Data.FirstOrDefault();

            return found?.ToDto();
        }
    }
}
