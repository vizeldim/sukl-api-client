﻿using System.Text.Json.Serialization;

namespace PharmacyApp.Data.Dto.SuklApi.RequestQuery
{
    public class DrugWhereFilter : WhereFilter
    {
        [JsonPropertyName("NAZEV")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public String? Name { get; set; }

        [JsonPropertyName("KOD_SUKL")]
        [JsonIgnore(Condition = JsonIgnoreCondition.WhenWritingNull)]
        public String? SuklCode { get; set; }
    }
}
