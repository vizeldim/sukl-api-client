# Description

API Client for SUKL API
- link: https://api.apitalks.store/sukl.cz

Solution contains .NET core Blazor server app
- Home Page Design: https://www.figma.com/file/03I5yRzwYRRrxVcXwsVZfu/Untitled
- Only data layer ('/Data') was implemented. 
