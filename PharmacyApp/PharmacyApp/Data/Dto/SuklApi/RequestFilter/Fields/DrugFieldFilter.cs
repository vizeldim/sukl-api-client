﻿using System.Text.Json.Serialization;

namespace PharmacyApp.Data.Dto.SuklApi.RequestQuery
{
    public class DrugFieldFilter : FieldFilter
    {
        [JsonPropertyName("NAZEV")]
        public bool Name { get; set; } = true;

        [JsonPropertyName("KOD_SUKL")]
        public bool SuklCode { get; set; } = true;
    }
}
