using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.FileProviders;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using PharmacyApp.Data.External;
using PharmacyApp.Data.External.SuklApi;
using Refit;
using System.Text.Json;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddServerSideBlazor();

//Change the default razor pages directory 'Pages' to the specified directory '/Presentation/Pages'
builder.Services.Configure<RazorPagesOptions>(options => options.RootDirectory = "/Presentation/Pages");


// Http Client registration & config for SUKL API
string apiKey = builder.Configuration["SuklApiKey"];
string apiBaseUrl = "https://api.apitalks.store/sukl.cz";


var _options = new JsonSerializerOptions()
{
    DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
  
};

var _settings = new RefitSettings()
{
    ContentSerializer = new SystemTextJsonContentSerializer(_options)

};
builder.Services.AddRefitClient<ISuklApiDescription>()
    .ConfigureHttpClient(httpClient =>
    {
        httpClient.BaseAddress = new Uri(apiBaseUrl);

        httpClient.DefaultRequestHeaders.Add("x-api-key", apiKey);

    });


builder.Services.AddSingleton<IDrugApiClient, SuklClient>();


var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseStaticFiles();
app.UseRouting();

app.MapBlazorHub();
app.MapFallbackToPage("/_Host");



//Change the default wwwroot to the specified directory so that the themes css and js can be designed independently
app.UseStaticFiles(
    new StaticFileOptions()
    {
        FileProvider = new PhysicalFileProvider(
            Path.Combine(
                Directory.GetCurrentDirectory(), 
                "Presentation", 
                "Theme")),

        RequestPath = new PathString("")
    }
);

app.Run();
