﻿using PharmacyApp.Data.Dto.SuklApi.RequestFilter;
using System.Text.Json;

namespace PharmacyApp.Data.Dto.SuklApi
{
    public static class SuklDataConverterExtensions
    {
        public static string ToQueryString(this DrugPageFilter? filter) => JsonSerializer.Serialize(filter);

        public static DrugDto ToDto(this SuklDrugDto suklDrug) =>
            new DrugDto
            {
                Name = suklDrug.Name,
                SuklCode = suklDrug.SuklCode
            };

        public static DrugPageDto ToDto(this SuklDrugPageDto suklDrug) =>
            new DrugPageDto
            {
                Data = suklDrug.Data.Select(x => x.ToDto()).ToList()
            };
    }
}
